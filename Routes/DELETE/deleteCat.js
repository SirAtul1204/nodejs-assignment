const express = require("express");
const deleteCat = express.Router();
const { removeCat } = require("../../DB/main");

deleteCat.delete("/:id", async (req, res) => {
  const id = parseInt(req.params.id);
  const data = await removeCat(id);
  if (data == "SUCCESS") {
    return res.status(200).send({ msg: "Data deleted successfully" });
  }
  return res.status(404).send({ msg: "Error!", error: "Record not found!" });
});

module.exports = deleteCat;
