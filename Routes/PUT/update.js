const express = require("express");
const update = express.Router();
const { updateById } = require("../../DB/main");

update.put("/:id", async (req, res) => {
  const id = parseInt(req.params.id);
  const { name, age, breed } = req.body;
  const data = await updateById(id, { name, age: parseInt(age), breed });
  if (data == "SUCCESS")
    return res.status(200).send({ msg: "Data updated successfully" });
  else return res.status(500).send({ msg: "Error!", error: data[1] });
});

module.exports = update;
