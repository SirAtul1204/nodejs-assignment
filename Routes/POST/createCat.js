const express = require("express");
const createCat = express.Router();
const { insert } = require("../../DB/main");

createCat.post("/", async (req, res) => {
  const { name, age, breed } = req.body;
  if (name && age && breed) {
    const data = await insert(name, parseInt(age), breed);
    if (data[0] == "SUCCESS") {
      return res.status(200).send({ msg: "Record added successfully!" });
    }
    return res.status(500).send({ msg: "Error!", error: data[1] });
  }
  return res.status(400).send({ msg: "Not all fields are present" });
});

module.exports = createCat;
