const express = require("express");
const getById = express.Router();
const { selectById } = require("../../DB/main");

getById.get("/:id", async (req, res) => {
  const id = parseInt(req.params.id);
  const data = await selectById(id);
  if (data[0] == "SUCCESS") {
    return res
      .status(200)
      .send({ msg: "Data retrieved successfully!", data: data[1] });
  }
  return res.status(404).send({
    msg: "Error!",
    error: data[1],
  });
});

module.exports = getById;
