const express = require("express");
const searchByAge = express.Router();
const { selectByAge } = require("../../DB/main");

searchByAge.get("/search", async (req, res) => {
  let ageL, ageH;
  if (req.query.age_lte) ageL = parseInt(req.query.age_lte);
  else
    return res
      .status(500)
      .send({ msg: "Error!", error: "Specify age_lte parameter" });
  if (req.query.age_gte) ageH = parseInt(req.query.age_gte);
  else
    return res
      .status(500)
      .send({ msg: "Error!", error: "Specify age_gte parameter" });

  const data = await selectByAge(ageL, ageH);
  if (data[0] == "SUCCESS")
    return res
      .status(200)
      .send({ msg: "Record retrieved successfully!", data: data[1] });
  else return res.status(500).send({ msg: "Error!", error: data[1] });
});

module.exports = searchByAge;
