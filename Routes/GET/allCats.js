const express = require("express");
const allCats = express.Router();
const { selectAll } = require("../../DB/main");

allCats.get("/", async (req, res) => {
  const data = await selectAll();
  if (data[0] == "SUCCESS") {
    return res
      .status(200)
      .send({ msg: "Data retrieved successfully!", data: data[1] });
  }
  return res.status(500).send({ msg: "Error!", error: data[1] });
});

module.exports = allCats;
