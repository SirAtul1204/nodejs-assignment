require("dotenv").config();

const express = require("express");
const app = express();
const { json } = require("body-parser");
const cors = require("cors");

const allCats = require("./Routes/GET/allCats");
const createCat = require("./Routes/POST/createCat");
const getById = require("./Routes/GET/getById");
const searchByAge = require("./Routes/GET/searchByAge");
const update = require("./Routes/PUT/update");
const deleteCat = require("./Routes/DELETE/deleteCat");

app.use(cors());
app.use(json());

app.use("/api", allCats);
app.use("/api", createCat);
app.use("/api", searchByAge);
app.use("/api", getById);
app.use("/api", update);
app.use("/api", deleteCat);

app.get("/", (req, res) => {
  res.status(200).send({ msg: "Node Assignment 1" });
});

app.listen(process.env.PORT, () => {
  console.log(`Application Ready ⚡\nListening on PORT ${process.env.PORT} ✅`);
});
