const SQLiteCrud = require("sqlite3-promisify");
const DB_PATH = __dirname + "/data.db";

let db;

(async () => {
  db = new SQLiteCrud(DB_PATH);
  await db.run(
    `CREATE TABLE IF NOT EXISTS cats(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, age NUMBER, breed TEXT);`
  );
})();

const insert = async (name, age, breed) => {
  try {
    await db.run("INSERT INTO cats(name, age, breed) VALUES(?, ?, ?)", [
      name,
      age,
      breed,
    ]);
    return ["SUCCESS"];
  } catch (e) {
    return ["FAIL", e];
  }
};

const selectAll = async () => {
  try {
    const rows = await db.all(`SELECT * FROM cats;`);
    return ["SUCCESS", rows];
  } catch (e) {
    return ["FAIL", e];
  }
};

const selectById = async (id) => {
  try {
    const record = await db.get(`SELECT * FROM cats WHERE id = ${id};`);
    if (record) return ["SUCCESS", record];
    else throw `Record not found`;
  } catch (e) {
    return ["FAIL", `Record not found for id = ${id}`];
  }
};

const selectByAge = async (ageL, ageH) => {
  try {
    const record = await db.all(
      `SELECT * FROM cats WHERE age >= ${ageL} AND age <= ${ageH};`
    );
    if (record.length > 0) return ["SUCCESS", record];
    else throw `Record not found!`;
  } catch (e) {
    return ["FAIL", `No records in the given age range (${ageL} - ${ageH})`];
  }
};

const updateById = async (id, { name, age, breed }) => {
  try {
    if (name)
      await db.run(`UPDATE cats SET name = '${name}' WHERE id = ${id};`);
    if (age) await db.run(`UPDATE cats SET age = ${age} WHERE id = ${id};`);
    if (breed)
      await db.run(`UPDATE cats SET breed = '${breed}' WHERE id = ${id};`);

    return ["SUCCESS"];
  } catch (e) {
    console.log(e);
    return ["FAIL", e];
  }
};

const removeCat = async (id) => {
  try {
    await db.run(`DELETE FROM cats WHERE id = ${id}`);
    return ["SUCCESS"];
  } catch (e) {
    return ["FAIL", e];
  }
};

module.exports = {
  insert,
  selectAll,
  selectById,
  selectByAge,
  updateById,
  removeCat,
};
